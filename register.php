<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="main.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    </head>
    <body>
        <?php
            $name = $gender = $select = $birthday = "";
            $nameErr = $genderErr = $selectErr = $birthdayErr = "";
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                if (empty($_POST["name"])) {
                    $nameErr = "Hãy nhập tên.";
                } else {
                    $name = test_input($_POST["name"]);
                }
                if (empty($_POST["gender"])) {
                    $genderErr = "Hãy chọn giới tính." ;
                } else {
                    $gender = test_input($_POST["gender"]);
                }
                if (empty($_POST["select"])) {
                    $selectErr = "Hãy chọn phân khoa." ;
                } else {
                    $select = test_input($_POST["select"]);
                }
                if (empty($_POST["birthday"])) {
                    $birthdayErr = "Hãy nhập ngày sinh." ;
                } else {
                    $birthday = test_input($_POST["birthday"]);
                    if (!validateBirthday($_POST["birthday"])) {
                        $birthdayErr = "Hãy nhập ngày sinh đúng định dạng";
                    }
                }
            }
            function validateBirthday($birthday){
                $birthdays  = explode('/', $birthday);
                if (count($birthdays) == 3) {
                    return checkdate($birthdays[1], $birthdays[0], $birthdays[2]);
                }
                return false;
            }
            function test_input($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
        ?>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
            <div class="container">
                <div class="container__content">
                    <div class="error">
                        <?php 
                            if (!empty($nameErr)) {
                                echo $nameErr ."<br />";
                            }
                            if (!empty($genderErr)) {
                                echo $genderErr ."<br />";
                            }
                            if (!empty($selectErr)) {
                                echo $selectErr ."<br />";
                            }
                            if (!empty($birthdayErr)) {
                                echo $birthdayErr ."<br />";
                            }
                        ?>
                    </div>
                    <div class="form__item">
                        <label for="name" class="form__label">
                            Họ và tên
                            <span>*</span>
                        </label>
                        <input type="text" class="form__input" id="name" name="name">
                    </div>
                    <div class="form__item">
                        <div class="form__label">
                            Giới tính
                            <span>*</span>
                        </div>
                        <!-- for loop -->
                        <?php
                            $gender = array(
                                0 => "Nam", 
                                1 => "Nữ"    
                            );
                            for($i = 0; $i < count($gender); $i++) {
                                echo    "<label class='form__gender'>
                                            $gender[$i]
                                            <input type='radio' name='gender' class='form__input input__gender'>
                                            <span class='checkmark'></span>
                                        </label>";
                            }
                        ?>
                    </div>
                    <div class="form__item">
                        <label for="select" class="form__label">
                            Phân khoa
                            <span>*</span>
                        </label>
                        <div class="select">
                            <select name="select" id="select" class="form__input form__select">
                                <!-- foreach loop -->
                                <?php
                                    $classes = array(
                                        "MAT" => "Khoa học máy tính",
                                        "KDL" => "Khoa học vật liệu"
                                    );
                                    echo "<option value=''></option>";
                                    foreach($classes as $class => $val) {
                                        echo "<option value='$class'>$val</option>";
                                    }
                                ?>
                            </select>
                            <span class="focus"></span>
                        </div>
                    </div>
                    <div class="form__item">
                        <label for="birthday" class="form__label">
                            Ngày sinh
                            <span>*</span>
                        </label>
                        <div class="form__birthday">
                            <div class="input-group">
                                <input class="form__input" id="birthday" name="birthday" placeholder="dd/mm/yyyy" type="text"/>
                            </div>
                        </div>
                    </div>
                    <div class="form__item">
                        <label for="address" class="form__label">
                            Địa chỉ
                        </label>
                        <input type="text" class="form__input" id="address" name="address">
                    </div>
    
                    <input type="submit" value="Đăng ký" class="form__submit">
                    
                </div>
            </div>
        </form>
        
        <script>
            $(document).ready(function(){
                var date_input=$('input[name="birthday"]');
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                    format: 'dd/mm/yyyy',
                    container: container,
                    todayHighlight: true,
                    autoclose: true,
                })
            })
        </script>
    </body>
</html>
